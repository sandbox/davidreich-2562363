EU Payments for Ubercart

INTRODUCTION
------------
The EU Payments for Ubercart module is a Payment method pack for Ubercart.
It is an easy to use and out of the box solution for people who want to use 
Ubercart in european countries.
It contains the most common non gateway payment methods used in the EU like
direct debit and bank transfer.



 * For a full description of the module, visit the project page:
   https://drupal.org/project/xxx


 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/xxx


REQUIREMENTS
------------
This module requires the following modules:
 * Ubercart (https://www.drupal.org/project/ubercart)


RECOMMENDED MODULES
-------------------
To put ubercart to full use in the EU use these recommend modules:
 * Ubercart Register Invoice Templates by DanZ 
 (https://www.drupal.org/project/uc_register_invoice): 
   Better overview for Invoice Templates and multiple templates when using
   Drupal multisites.
 * Ubercart Addresses by MegaChriz 
 (https://www.drupal.org/project/uc_addresses):
   Recommended for pleaseant user experience and address standarts in different
   countries.
   Also needed if you want to create Bank account fields in the address block.
 * Ubercart Terms of Service by pcambra 
 (https://www.drupal.org/project/uc_termsofservice):
   Agreement on ToS are required in most european countries.


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Once installed and activated, make sure to mark the needed payment method in 
   Ubercart in Administration � Store � Configuration � Payment methods
 * In Administration � Store � Configuration � Payment methods you can either 
   configure the Bank transfer methode 
   (Setting a policy message and own bank informations)
   and the Direct debit methode (Policy message and if the input of the 
   bank account should be made right under the payment method.


MAINTAINERS
-----------
Current maintainers:
 * David U. Reich (DavidReich) - https://www.drupal.org/user/3295533
