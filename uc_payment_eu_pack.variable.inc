<?php

/**
 * @file
 * Variable module hook implementations.
 */

/**
 * Implements hook_variable_group_info().
 */
function uc_payment_eu_pack_variable_group_info() {
  $groups['uc_payment_pack_eu'] = array(
    'title' => t('Ubercart payment EU pack settings'),
    'access' => 'administer store',
    'path' => array('admin/store/settings/payment/method'),
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function uc_payment_eu_pack_variable_info($options) {
  $variables['uc_bt_policy'] = array(
    'type' => 'text',
    'title' => t('Bank transfer policy message', array(), $options),
    'description' => t('Policy message for Bank transfer', array(), $options),
    'group' => 'uc_payment_pack_eu',
    'default' => t('Full payment is expected as bank transfer in advance to this account:', array(), $options),
  );
  $variables['uc_bt_iban'] = array(
    'type' => 'text',
    'title' => t('IBAN', array(), $options),
    'description' => t('Your Bank Account IBAN', array(), $options),
    'group' => 'uc_payment_pack_eu',
    'default' => t('Insert IBAN here', array(), $options),
  );
  $variables['uc_bt_bic'] = array(
    'type' => 'text',
    'title' => t('BIC', array(), $options),
    'description' => t('Your Bank Account BIC', array(), $options),
    'group' => 'uc_payment_pack_eu',
    'default' => t('Insert BIC here', array(), $options),
  );
  $variables['uc_debit_policy'] = array(
    'type' => 'text',
    'title' => t('Direct debit payment policy', array(), $options),
    'description' => t('Policy message for direct debit.', array(), $options),
    'group' => 'uc_payment_pack_eu',
    'default' => t('Payment as direct debit. Only in the EU.', array(), $options),
  );
  return $variables;
}
